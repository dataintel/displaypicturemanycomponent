import { Component, OnInit,Output, Input, EventEmitter, ViewContainerRef, ViewEncapsulation  } from '@angular/core';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-basecomponent',
  template:`
  <style>
  /** latest tweet */

.list-tweets {
    padding-right: 10px;
    background: transparent;
    margin: 0;
    height: 305px;
    overflow-y: auto;
}

.list-tweets:after {
    display: block;
    content: "";
    clear: both;
}

.list-tweets .list-group-item {
    padding: 10px 0;
    background: transparent;
    border-left: none;
    border-right: none;
    border-color: #ddd;
    font-size: 13px;
}

.list-tweets>span:first-of-type {
    border-top: none;
}

.list-tweets>span:last-of-type {
    border-bottom: none;
}

.list-tweets .list-group-item .content-tweet {
    padding-left: 58px;
}

.list-tweets .list-group-item .content-tweet .profile-picture,
.list-tweets .list-group-item .content-tweet .profile-picture-default {
    width: 48px;
    height: 48px;
}

.list-tweets .list-group-item .content-tweet>a {
    color: #263238;
}

.list-tweets .list-group-item .content-tweet>a:hover {
    text-decoration: none;
}

.list-tweets .list-group-item .content-tweet>a:hover>strong {
    text-decoration: underline;
}

.list-tweets .list-group-item .content-tweet>a>img {
    float: left;
    margin-left: -58px;
    border-radius: 5px;
}

.list-tweets .list-group-item .content-tweet>p {
    margin-bottom: 5px;
    color: #263238;
}

.list-tweets .list-group-item .content-tweet [class^=tweet-image]:before,
.list-tweets .list-group-item .content-tweet [class^=tweet-image]:after {
    display: block;
    content: " ";
    clear: both;
}

.list-tweets .list-group-item .content-tweet .post-date {
    white-space: nowrap;
    font-size: .9em;
}

.list-tweets .list-group-item .content-tweet .source .media .fa {
    font-size: 1.2em;
    cursor: default;
}

.list-tweets .list-group-item .content-tweet .source {
    font-size: 0.9em;
    margin: 5px 0 0;
    color: #666;
}

.list-tweets .list-group-item .content-tweet .source .fa-retweet {
    color: #5C913B;
}

.list-tweets .list-group-item .content-tweet .text-muted {
    color: #666;
}

.news-detail {
    height: 200px;
    overflow: hidden;
}

.news-detail .news-detail-img {
    float: left;
    padding: 0;
     width: 40%;
    height: 180px;
   margin-right: 10px;
    overflow: hidden;
    background: #ccc no-repeat center center;
    background-size: cover;
    width:250px;
    height:250px;
}

.list-image-news {
    overflow: hidden;
    position: relative;
}

#influencer-mention-in-news {
    height: 200px;
    padding-top: 2px;
}

.panel .panel-body {
    padding: 5px 0;
}

.list-image-news .pict {
    float: left;
    margin-right: 5px;
    margin-bottom: 5px;
    width: calc(16% - 5px);
    height: 45px;
    background: #eee no-repeat center center;
    background-size: cover;
    border: 1px solid #ccc;
    cursor: pointer;
    border-radius: 4px;
    margin-left: 10px;
    width: 50px;
    height: 50px;
}

.news-detail .news-detail-img2 {
    float: left;
    width: 50px;
    height: 50px;
    margin-right: 10px;
    overflow: hidden;
    background: #ccc no-repeat center center;
    background-size: cover;
}
.picture-detail{
    cursor: pointer;    
}
.picture-detail :hover{    
    background-color: #f5f5f5;
    color: #555;
}
  </style>
  <!--<div [ngBusy]="news_picture_many_busy" id="influencer-mention-in-news" class="panel-body">-->

    <div *ngIf="items?.length > 0" class="news-detail news-detail-wrapper-5835207167829">
        <div class="news-detail-img col-md-5" [ngStyle]="{'background-image': 'url(' + image +'),url(assets/foto1.jpg)'}"></div>
        <div class="col-md-7">
            <!--list iemage yang 5 gambar-->
            <div class="list-image-news">
                <div *ngFor="let subitem of items; let i=index" (click)="onSelect(subitem)">
                    <template [ngIf]="i<12">
                        <div class="pict" [ngStyle]="{'background-image': 'url(' + subitem.image + '),url(assets/foto2.jpg)'}"></div>
                    </template>
                </div>
            </div>
            <!--picture detail ini yg ada gambar sama flashbacknya gitu detail textnya-->
            <div class="picture-detail">
                <h5 class="colored-news-title">
                    <strong>Flash Back :</strong>
                    <strong class="text-justify" [innerHtml]="title"></strong></h5>
                <p class="text-justify" [innerHtml]="content"></p>
            </div>
        </div>
    </div>
    <!--<div class="text-md-center" *ngIf="items?.length == 0">
        No Result
    </div>-->
<!--</div>-->
  `
})
export class BaseWidgetDisplayPictureManyComponent implements OnInit {
 public image;
  public title;
 public content;
  public items: any;

  constructor() {

  }
  ngOnInit() {
    this.items = 

    [
        {
            "image": "assets/foto1.jpg",
            "title":"Air enting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil�) tersedia di Bumi",
            "content":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil�) tersedia di Bumi."
           
        },
        {
            "image": "assets/foto2.jpg",
            "title":"Api",
             "content":"API (Application Programming Interface) adalah sekumpulan perintah, fungsi, komponen, dan protokol yang disediakan oleh sistem operasi ataupun bahasa pemrograman tertentu yang dapat digunakan oleh programmer saat membangun perangkat lunak"
           
        },
         {
            "image": "assets/foto3.jpg",
            "title":"Udara",
          "content":"Udara adalah suatu campuran gas yang terdapat pada lapisan yang mengelilingi bumi."
        },
         {
            "image": "assets/foto4.jpg",
            "title":"Oksigen",
            "content":"Oksigen atau zat asam adalah unsur kimia dalam sistem tabel periodik yang mempunyai lambang O dan nomor atom 8. Ia merupakan unsur golongan kalkogen dan dapat dengan mudah bereaksi dengan hampir semua unsur lainnya (utamanya menjadi oksida). ... Gas oksigen diatomik mengisi 20,9% volume atmosfer bumi"
        },
         {
            "image": "assets/foto5.jpg",
            "title":"kehidupan",
           "content":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil�) tersedia di Bumi.",
           
        },
    ];
     this.onSelect(this.items[0]);
  }

  //buat load data baru
  ngOnChanges() {
    this.items = [];
  }

  onSelect(subitem) {
    if (subitem) {
      this.image = subitem.image;
      this.title = subitem.title;
      this.content = subitem.content;

    }
  }
}
