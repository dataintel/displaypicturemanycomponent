import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetDisplayPictureManyComponent } from './src/basecomponent.component';

export * from './src/basecomponent.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
   
	BaseWidgetDisplayPictureManyComponent
  ],
  exports: [
    
	BaseWidgetDisplayPictureManyComponent
  ]
})
export class DisplayPictureManyModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DisplayPictureManyModule,
      providers: []
    };
  }
}
