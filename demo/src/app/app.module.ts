import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { DisplayPictureManyModule } from '../../../index';

// import {BusyModule} from 'angular2-busy'; 
@NgModule({
  declarations: [
    AppComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DisplayPictureManyModule
    // BusyModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
